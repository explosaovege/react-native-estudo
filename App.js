/**
 * Sample React Native App
 * https://github.com/facebook/react-native
 *
 * @format
 * @flow
 */

import Simples from './componentes/Simples';

import ParImpar from './componentes/ParImpar';

import {Inverter, MegaSena} from './componentes/Multi';

export default class App extends Component {
  render(){
    return 
      <View style={Style.container}>
        <Simples texto="Parametro"/>
        <ParImpar numero={22} />
        <Inverter texto="React-Native" />
        <MegaSena numeros={6} />
      </View>
  }
}

const styles = StyleSheet.create ({
  container: {
    flex: 1,
    justifyContent: 'center',
    alignItems: 'center',
  },
});